<?php

abstract class StorageType {
    const IO = 1;
    const FastDFS = 2;
    const Minio = 3;
    const OSS = 4;
 }
?>