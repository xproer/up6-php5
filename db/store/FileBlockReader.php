<?php

/*
	文件块数据读取类
		
	更新记录：
        2022-05-16 创建
*/
class FileBlockReader
{
    public $storage;//读取器类型
    
	function __construct() 
	{
	    $this->storage = StorageType::IO;
	}	

	function read($path,$offset,$size)
	{
	    if(!file_exists($path)) throw new \Exception("file not exist:".$path);
	    
        $h = fopen($path,"rb");
        fseek($h,$offset);
        $data = fread($h,$size);
        fclose($h);
        return $data;
	}
}
?>