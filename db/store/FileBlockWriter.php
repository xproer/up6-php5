<?php
/*
	文件续传对象。负责将文件块写到服务器指定目录中
		
	更新记录：
	   2019-05-22
	       优化。
		2015-03-16
			修改创建文件的逻辑。将按实际文件大小创建一个大文件改为只创建一个字节的小文件，减少用户等待时间。
		2014-04-09 
			修复Resumer方法中调用unlink($tmpPath);警告。
			新增CreateFile方法。
			增加文件块验证功能。
		2014-03-21 取消创建临时文件的操作，减少一次系统IO，直接读取临时文件。
		2012-03-30 创建
*/
class FileBlockWriter
{	
	public $storage;//写入器类型
	public $m_lenLoc;//文件总大小。

	function __construct() 
	{
		$this->storage = StorageType::IO;
		$this->m_lenLoc=0;
	}

	function make(/*FileInf*/$file){
		$pathSvr = $file->pathSvr;
		//Windows平台需要转成GBK
		if(StringTool::iequals(PHP_OS,"WINNT")) 
		{
			$pathSvr = iconv('UTF-8', 'GBK', $pathSvr);
		}

		//创建层级目录
		$fd = dirname($pathSvr);

		if( !file_exists($fd) || !is_dir($fd)) mkdir($fd,0777,true);
		
		$hfile = fopen($pathSvr,"wb");
		//以原始大小创建文件
		ftruncate($hfile,$file->lenLoc);
		if($file->encrypt) ftruncate($hfile,$file->lenLocSec);
		fclose($hfile);
		
		if(!file_exists($pathSvr)) throw new \Exception("make file error");
		return "";
	}

	/**
	 * 向文件中写入块数据
	 */
	function write(/*FileInf*/$file,$data){
		$pathSvr = $file->pathSvr;

		//Windows平台需要转成GBK
		if(StringTool::iequals(PHP_OS,"WINNT")) 
		{
			$pathSvr = iconv('UTF-8', 'GBK', $pathSvr);
		}

	    //写入数据
	    $hfile = fopen($pathSvr,"r+b");
	    //使用块加密偏移
	    if($file->encrypt) fseek($hfile, intval($file->blockOffsetCry),SEEK_SET);
	    else fseek($hfile, intval($file->blockOffset),SEEK_SET);
	    fwrite($hfile,$data);
	    fclose($hfile);
	    
	    return $file->pathSvr;
	}
	
	function writeLastPart(/*FileInf*/$file){}
}
?>