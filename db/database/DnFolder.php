<?php

/*
 * 
	更新记录：
		2016-07-31 创建
		2017-07-25 更新
*/
class DnFolder
{
	function __construct()
	{ 
	}

	function Clear()
	{
	    $se = new SqlExec();
	    $se->exec("down_files", "truncate table down_files", null, null, null);		
	}
	
	function all_file($id)
	{
	    $se = new SqlExec();
	    $data = $se->exec_arr("up6_files"
	        , "select f_id,f_nameLoc ,f_pathSvr ,f_pathRel ,f_lenSvr ,f_sizeLoc from up6_files where f_pidRoot='$id';"
	        , "f_id,f_nameLoc,f_pathSvr,f_pathRel,f_lenSvr,f_sizeLoc"
	        , "f_id,nameLoc,pathSvr,pathRel,lenSvr,sizeLoc");
		
	    return json_encode($data,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
	
	/**
	 * 获取指定目录下的所有子文件
	 */
	function files($id)
	{
	    $se = new SqlExec();
	    $fd = $se->read("up6_folders", "f_pidRoot", array("f_id"=>$id));
	    $pidRoot = "";
	    if( empty($fd) ) $pidRoot = $id;
	    else 
	    {
	        $pidRoot = trim($fd["f_pidRoot"]);
	    }
	    
	    return $this->filesChild($id, $pidRoot);
	}
	
	function filesChild($id,$pidRoot)
	{
	    $prb = new PathRelBuilder();
	    $fs = $prb->build($id, $pidRoot);
	    return json_encode($fs,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
	
	function childs($pidRoot)
	{
	    $sql = "select f_id, f_lenSvr, f_lenLocSec, f_nameLoc, f_pathSvr, f_pathRel, f_encrypt, f_encryptAgo, f_blockSize, f_blockSizeSec from up6_files where f_pidRoot='$pidRoot';";
	    $se = new SqlExec();
	    $data = $se->exec_arr("up6_files"
	        , $sql
	        , "f_id,f_lenSvr,f_lenLocSec,f_nameLoc,f_pathSvr,f_pathRel,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec"
	        , "f_id,lenSvr,lenLocSec,nameLoc,pathSvr,pathRel,encrypt,encryptAgo,blockSize,blockSizeSec");
	    
	    for($i=0,$l=count($data);$i<$l;++$i)
	    {
	        $data[$i]["lenSvrSec"] = $data[$i]["lenLocSec"];
	    }
	    
	    return json_encode($data,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
}
?>