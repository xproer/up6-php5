<?php

/*
	文件夹操作类		
	更新记录：
		2014-08-12 创建
		2019-05-22 精简代码
*/
class DBFolder
{
	/**
	 * 将文件夹上传状态设为已完成
	 * 更新文件表
	 * 更新文件夹表
	 * @param unknown $id	文件ID
	 * @param unknown $uid
	 */
	function Complete($id,$uid)
	{
		$sql = "update up6_files set f_lenSvr=f_lenLoc,f_complete=1,f_perSvr='100%' where (f_id=:f_id or f_pidRoot=:f_id) and f_uid=:f_uid;";
		$se = new SqlExec();
		$se->exec("up6_files", $sql, null, "f_id,f_uid", array("f_id"=>$id,"f_uid"=>$uid));
		$se->update2("up6_folders", array("f_complete"=>1), array("f_id"=>$id,"f_uid"=>$uid));		
	}

	function Remove($fid,$uid)
	{
	    $se = new SqlExec();
	    $se->update2("up6_files", array("f_deleted"=>1), array("f_id"=>$fid,"f_uid"=>$uid));
	    $se->update2("up6_files", array("f_deleted"=>1), array("f_pidRoot"=>$fid,"f_uid"=>$uid));
	    $se->update2("up6_folders", array("f_deleted"=>1), array("f_id"=>$fid,"f_uid"=>$uid));		
	}

	static function Clear()
	{
	    $se = new SqlExec();
	    $se->exec("up6_folders", "delete from up6_folders", null, null, null);
	}

	function addBatch(/*array(FileInf)*/$arr){
		$ret = false;
		$sb = "insert into up6_folders(
			f_id,
			f_pid,
		 	f_pidRoot,
		 	f_nameLoc,
		 	f_uid,
		 	f_pathLoc,
		 	f_pathSvr,
		 	f_pathRel,
		 	f_complete
		 ) values(
			:f_id,
			:f_pid,
			:f_pidRoot,
			:f_nameLoc,
			:f_uid,
			:f_pathLoc,
			:f_pathSvr,
			:f_pathRel,
			:f_complete
		)";
	
		$db = new DbHelper();
	
		$cmd = $db->prepare_utf8($sb);
	
		foreach($arr as $f)
		{
		    $cmd->bindValue(":f_id", $f->id);
			$cmd->bindValue(":f_pid", $f->pid);
			$cmd->bindValue(":f_pidRoot", $f->pidRoot);
			$cmd->bindValue(":f_nameLoc", $f->nameLoc);
			$cmd->bindValue(":f_uid", $f->uid);
			$cmd->bindValue(":f_pathLoc", $f->pathLoc);
			$cmd->bindValue(":f_pathSvr", $f->pathSvr);
			$cmd->bindValue(":f_pathRel", $f->pathRel);
			$cmd->bindValue(":f_complete", 1,PDO::PARAM_BOOL);
			
			$db->ExecuteRow($cmd);
		}
	}
}
?>