<?php

/*
 * 
	更新记录：
		2016-07-31 创建
		2017-07-25 更新
*/
class DnFile
{
	var $db;//全局数据库连接,共用数据库连接
	
	function __construct()
	{
		$this->db = new DbHelper();
	}

	function Add($inf/*DnFileInf*/)
	{
	    $se = new SqlExec();
	    $se->insert("down_files"
	        , array("f_id"=>$inf->id
	            ,"f_uid"=>$inf->uid
	            ,"f_nameLoc"=>$inf->nameLoc
	            ,"f_pathLoc"=>$inf->pathLoc
	            ,"f_lenSvr"=>$inf->lenSvr
	            ,"f_sizeSvr"=>$inf->sizeSvr
	            ,"f_fdTask"=>$inf->fdTask
	           )
	        );
	}

	/// <summary>
	/// 删除文件
	/// </summary>
	/// <param name="fid"></param>
	function Delete($fid,$uid)
	{
	    $se = new SqlExec();
	    $se->delete("down_files", array("f_id"=>$fid,"f_uid"=>$uid));
	}

	/**
	 * 更新文件进度信息
	 * @param fid
	 * @param uid
	 * @param mac
	 * @param lenLoc
	 */
	function updateProcess($fid,$uid,$lenLoc,$perLoc)
	{
	    $se = new SqlExec();
	    $se->update2("down_files", array("f_lenLoc"=>$lenLoc,"f_perLoc"=>$perLoc), array("f_id"=>$fid,"f_uid"=>$uid));
	}
	
	function clear()
	{
	    $se = new SqlExec();
	    $se->exec("down_files", "truncate table down_files", "", null, null);		
	}
	
	function all_complete($uid)
	{
	    $se = new SqlExec();
		$sql = "select 
				 f_id
				,f_fdTask
				,f_nameLoc
				,f_sizeLoc
				,f_lenSvr
				,f_pathSvr
				,f_lenLocSec
				,f_encrypt
                ,f_encryptAgo
				,f_blockSize
                ,f_blockSizeSec
				,f_object_id
				 from up6_files
				 where f_uid=$uid and f_deleted=0 and f_fdChild=0 and f_complete=1 and f_scan=1;";
		
		$data = $se->exec_arr("up6_files", $sql,
		    "f_id,f_fdTask,f_nameLoc,f_sizeLoc,f_lenSvr,f_pathSvr,f_lenLocSec,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec,f_object_id",
		    "f_id,fdTask,nameLoc,sizeLoc,lenSvr,pathSvr,lenLocSec,encrypt,encryptAgo,blockSize,blockSizeSec,object_id");
		
		$sys = new SysTool();
		for($i=0,$l=count($data);$i<$l;++$i)
		{
		    $data[$i]["id"] = $sys->guid();
		    $data[$i]["sizeSvr"] = $data[$i]["sizeLoc"];
		    $data[$i]["lenSvrSec"] = $data[$i]["lenLocSec"];
		    $data[$i]["lenLocSec"] = 0;
		}
		return json_encode($data,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
	
	/**
	 * 加载所有未下载完的文件和文件夹
	 * @param unknown $uid
	 * @return string
	 */
	function all_uncmp($uid)
	{
		$sql = "select 
				 f_id
				,f_nameLoc
				,f_pathLoc
				,f_perLoc
				,f_sizeSvr				
				,f_fdTask
				 from down_files
				 where f_uid=$uid and f_complete=0;";
		$se = new SqlExec();
		$data = $se->exec_arr("down_files"
		    , $sql
		    , "f_id,f_nameLoc,f_pathLoc,f_perLoc,f_sizeSvr,f_fdTask"
		    , "id,nameLoc,pathLoc,perLoc,sizeSvr,fdTask");		
		
		return json_encode($data,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
}
?>