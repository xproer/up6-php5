<?php

class SqlCmdReader
{
    //构造函数
    function __construct()
    {
    }
    
    //析构函数
    function __destruct()
    {
    }
    
    /**
     * 
     * @param $row 结果集（行）
     * @param $type 字段类型
     * @param $name 字段名称
     * @return 值
     */
    function read($row,$type,$name)
    {
        if( strcasecmp($type,"string") == 0 ) return $row[$name];
        else if( strcasecmp($type,"int") == 0 ) return (int)$row[$name];
        else if( strcasecmp($type,"datetime") == 0 ) return $row[$name];
        else if( strcasecmp($type,"long") == 0 ) return (int)$row[$name];
        else if( strcasecmp($type,"smallint") == 0 ) return (int)$row[$name];
        else if( strcasecmp($type,"tinyint") == 0 ) return (int)$row[$name];
        else if( strcasecmp($type,"short") == 0 ) return (int)$row[$name];
        else if( strcasecmp($type,"byte") == 0 ) return $row[$name];
        else if( strcasecmp($type,"bool") == 0 ) return (bool)$row[$name];
    }
    
    function readField($row,$field/*字段信息,json数组*/)
    {        
        $name = $field["name"];
        $type = $field["type"];
        return $this->read($row, $type, $name);
    }
    
    /**
     * 
     * @param $row 数据行
     * @param $fields 字段列表
     * @return array id=>1,name=>a
     */
    function readFields($row,$fields)
    {
        $arr = array();
        for($i = 0 , $l = count($fields) ; $i<$l;++$i)
        {
            $field = $fields[$i];
            $name = $field["name"];
            $type = $field["type"];
            $val = $this->read($row, $type, $name);
            $arr[$name] = $val;
        }
        return $arr;
    }
}
?>