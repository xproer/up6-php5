<?php

class SqlWhereMerge
{
    var $m_cds;//关联数组
    
    //构造函数
    function __construct()
    {
        $this->m_cds = array();
    }
    
    //析构函数
    function __destruct()
    {
    }
    
    function equal($n,$v)
    {
        if( is_int($v)) $this->m_cds[$n] = "$n=$v";
        else if( is_string($v)) $this->m_cds[$n] = "$n='$v'";
    }
    
    /**
     * 
     * @param unknown $n
     * @param unknown $v
     * @param unknown $igo 是否忽略空值
     */
    function equalIgore($n,$v,$igo)
    {
        if($v=="")
        {
            if($igo) return;
        }
        $this->m_cds[$n]="$n='$v'";
    }
    
    function add($n,$v)
    {
        if(array_key_exists($n, $this->m_cds)) $this->m_cds = array_diff_key($this->m_cds, array($n=>""));
        $this->m_cds[$n] = $v;
    }
    
    function instr($n,$column)
    {
        $sql = "instr($column,'$n')=1";
        $this->m_cds[""] = $sql;
    }
    
    function like($n,$v)
    {
        $this->m_cds[$n] = "$n like '%$v%'";
    }
    
    function to_sql()
    {
        $arr = array();
        foreach($this->m_cds as $ak => $av)
        {
            $arr[] = $av;
        }
        return join(" and ",$arr);
    }
    
    function del($k)
    {
        $this->m_cds = array_diff_key($this->m_cds, array($k=>""));
    }
}
?>