<?php

class SqlValSetter
{
    function __construct()
    {
    }
    
    function __destruct()
    {
    }
    
    /**
     * 为cmd设置值
     * @param $cmd  命令对象
     * @param $fields 字段列表
     * @param $val 关联数组
     */
    public function setVal($cmd,$fields,$val)
    {
        if( empty($fields) ) return;
        if( empty($val) ) return;
        
        for($i = 0 , $l = count($fields) ; $i < $l ; ++$i)
        {
            $field = $fields[$i];
            $name = $field["name"];
            $type = $field["type"];
            $len  = (int)$field["length"];
            
                 if(  strcasecmp($type,"bool") == 0) $cmd->bindValue(":$name", (bool)$val[$name],\PDO::PARAM_BOOL);
            else if(  strcasecmp($type,"int") == 0) $cmd->bindValue(":$name", (int)$val[$name],\PDO::PARAM_INT);
            else if(  strcasecmp($type,"string") == 0) $cmd->bindValue(":$name", $val[$name],\PDO::PARAM_STR);
            else $cmd->bindValue(":$name", $val[$name]);
        }
    }
}
?>