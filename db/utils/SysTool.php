<?php

class SysTool
{   
    /*
     * 判断是否是关联数组
     */
    function isAssocArray($arr)
    {
        if( is_string($arr) ) return false;
        if( is_int($arr) ) return false;
        if( empty($arr) ) return false;
        
        foreach (array_keys($arr) as $key) 
        {
            if (is_int($key)) 
            {
                return false;
            }
            return true;
        }
        return true;
    }
    
    function startsWith($haystack, $needle){
        return strncmp($haystack, $needle, strlen($needle)) === 0;
    }
    
    function endsWith($haystack, $needle){
        return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }
    
    /**
     * 生成GUID
     * @return string
     */
    function guid()
    {
        $ret = "";
        if (function_exists('com_create_guid'))
        {
            $ret = com_create_guid();
        }
        else
        {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
            $ret = $uuid;
        }
        $ret = str_replace("{","",$ret);
        $ret = str_replace("}","",$ret);
        $ret = str_replace("-","",$ret);
        $ret = strtolower($ret);
        return $ret;
    }
    
    function trim($str)//删除所有的空格
    {
        $qian=array(" ","　","\t","\n","\r");
        $hou=array("","","","","");
        return str_replace($qian,$hou,$str);
    }
    
    function str_encode_check()
    {
        
    }
    
    function to_utf8($str)
    {
        $encode = mb_detect_encoding($str, array("ASCII",'UTF-8',"GB2312","GBK",'BIG5'));
        //是utf8
        if( strcasecmp('utf-8',$encode) == 0 ) return $str;
        if( strcasecmp('ascii',$encode) == 0 ) return $str;
        
        if( strcasecmp('gbk',$encode) == 0 ) return iconv("GBK", "UTF-8", $str);
        if( strcasecmp('gb2312',$encode) == 0 ) return iconv("GB2312", "UTF-8", $str);
        if( strcasecmp('EUC-CN',$encode) == 0 ) return iconv("GB2312", "UTF-8", $str);
    }
    
    function to_gbk($str)
    {
        $encode = mb_detect_encoding($str, array("ASCII",'UTF-8',"GB2312","GBK",'BIG5'));
        //是gbk
        if( strcasecmp('gbk',$encode) == 0 ) return $str;
        if( strcasecmp('EUC-CN',$encode) == 0 ) return $str;
        if( strcasecmp('ascii',$encode) == 0 ) return iconv("utf-8", "gbk", $str);
        
        if( strcasecmp('utf-8',$encode) == 0 ) return iconv("utf-8", "gbk", $str);
    }
}
?>