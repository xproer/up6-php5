<?php
/**
* 自定义一个异常处理类
*/
class Up6Exception extends Exception
{
   // 重定义构造器使 message 变为必须被指定的属性
   public function __construct($message,$arr=null, $code = 0) {
       var_dump($arr);

       // 确保所有变量都被正确赋值
       parent::__construct($message, $code);
   }

   // 自定义字符串输出的样式
   public function __toString() {
       return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
}