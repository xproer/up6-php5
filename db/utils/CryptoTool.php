<?php

class CryptoTool
{
	var $key = "2C4DD1CC9KAX4TA9";
	var $iv = "2C4DD1CC9KAX4TA9";
	
	function decrypt($txt)
	{
	    //16字节对齐
	    $str_padded = $txt;
	    if (strlen($str_padded) % 16) {
	        $str_padded = str_pad($str_padded,strlen($str_padded) + 16 - strlen($str_padded) % 16, "\0");
	    }
		//解密
		$encryptedData = base64_decode($str_padded);
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, $encryptedData, MCRYPT_MODE_CBC, $this->iv),"\x00..\x1F");
		
		return 	$decrypted;
	}

	function encrypt($txt)
	{
		//16字节对齐
		$str_padded = $txt;
		if (strlen($str_padded) % 16) {
			$str_padded = str_pad($str_padded,strlen($str_padded) + 16 - strlen($str_padded) % 16, "\0");
		}
	    //加密
	    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, $str_padded, MCRYPT_MODE_CBC, $this->iv));
	
	    return 	$encrypted;
	}
	
	function decode($txt)
	{
	    //解密
	    $ft = new FileTool();
	    $data = $ft->readBytes($txt);
	    $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, $data, MCRYPT_MODE_CBC, $this->iv);
	    $decrypted = substr($decrypted, 0);
	    return 	$decrypted;

	}
	
	/**
	 * aligo 加密算法：aes,sm4
	 */
	function decrypt2($aligo,$block)
	{
	    if(StringTool::iequals($aligo,"sm4/ecb")) return $this->sm4_decode($block);
	    if(StringTool::iequals($aligo,"sm4")) return $this->sm4_decode($block);
	    if(StringTool::iequals($aligo,"aes/ecb")) return $this->aes_ecb_decode($block);
	    return $this->aes_decode($block);
	}
	
	//aes/cbc/pkcs
	function aes_encode($block)
	{
		$aes = new OpenSSL_AES_Encrypt();
		return $aes->encrypt($block, $this->key, $this->iv);
	}
	
	//aes/cbc/pkcs
	function aes_decode($block)
	{
		$aes = new AES_Encrypt();
		return $aes->decrypt($block, $this->key, $this->iv);
	}
	
	function aes_ecb_decode($block)
	{
	    
	}
	
	function sm4_encode($s)
	{
		$sm4 = new Sm4Helper();
		return $sm4->encrypt($this->key,$s);
	}
	
	function sm4_decode($s)
	{
		$sm4 = new Sm4Helper();
		return $sm4->decrypt($this->key,$s);
	}
	
	function token($f,$action)
	{
		$str = $f->id . $f->nameLoc . $action;
		if($action == "block") $str = $f->id . $f->pathSvr . $action;
		$str = md5($str);
		$str = $this->encrypt($str);
		return $str;
	}
}
?>