<?php

class WebBase
{
    var $m_webCfg;
    var $m_path;
    var $m_param;
    
    function __construct()
    {
        $this->m_webCfg = new ConfigReader();
        $this->m_param = array();
        //注册请求变量
        $this->m_param["query"]=json_encode($_GET);
    }
    
    function __destruct()
    {
    }
    
    function paramPage()
    {
        $p = json_encode($this->m_param,JSON_UNESCAPED_SLASHES |JSON_NUMERIC_CHECK|JSON_UNESCAPED_UNICODE );
        echo "<script>var page=$p;</script>";
    }
    
    function toInclude($file)
    {
        $sys = new SysTool();
        if( $sys->endsWith($file, ".css") )
        {
            return "<link rel=\"stylesheet\" type=\"text/css\" href=\"$file\" />";
        }
        else return "<script type=\"text/javascript\" src=\"$file\" charset=\"utf-8\"></script>";
    }
    
    function path($name)
    {
        return $this->m_path[$name];
    }
    
    function res()
    {
        $arr = array();
        $args = func_get_args();
        for($i = 0,$l=count($args); $i < $l; ++$i)
        {
            if( is_string($args[$i]))
            {
                
                $arr[] = $this->toInclude($args[$i]);
            }
            else if( is_array($args[$i]))
            {
                $item = $args[$i];
                for($ai=0,$al=count($item);$ai<$al;++$ai)
                {
                    $arr[] = $this->toInclude($item[$ai]);
                }
            }
        }
        return join("",$arr);
    }
    
    function toContent($par)
    {
        echo $par;
        die();
    }
    
    /**
     * 
     * @param string $n
     * @return string
     */
    function queryString($n)
    {
        if( isset($_GET[$n]) ) return trim($_GET[$n]);
        return "";
    }
    
    function reqString($n,$def)
    {
        if( isset($_GET[$n]) ) return trim($_GET[$n]);
        return $def;
    }
    
    function reqStringDecode($n)
    {
        $v = $this->queryString($n);
        return PathTool::urldecode_path($v);
    }
    
    function reqInt($n)
    {
        $v = $this->queryString($n);
        if($v == "") return 0;
        return intval($v);
    }
    
    function reqBool($n)
    {
        $v = $this->queryString($n);
        if($v == "") return false;
        return strcasecmp("true",$v)==0;
    }
}
?>