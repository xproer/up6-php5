<?php

class ConfigReader
{
    var $m_files;
    var $m_appPath;//项目路径 /www/up6/
    
    
    //构造函数
    function __construct()
    {
        //定位到/config/
        $root = str_replace("\\", "/", __FILE__);
        $pos = strpos($root,"/db/utils/");        
        //
        $root = substr($root, 0,$pos+1);
        //=>web根目录
        $this->m_appPath = $root;
        
        $ps = $this->m_appPath . "config/config.json";
        
        $data = FileTool::readAll($ps);
        $this->m_files = json_decode($data,true);
    }
    
    //析构函数
    function __destruct()
    {
    }
    
    function module($name)
    {
        $p = $this->m_files[$name];
        $p = $this->m_appPath . $p;
        $data = FileTool::readAll($p);
        return json_decode($data,true);
    }
    
    function readLine($data,$name)
    {
        $p = $data[$name];
        
        return $p;
    }
    
    function data($name)
    {
        $p = $this->m_files[$name];
        $p = $this->m_appPath . $p;
        $data = FileTool::readAll($p);
        return $data;
    }
    
    function dataPath($name)
    {
        $p = $this->m_appPath . $name;
        $data = FileTool::readAll($p);
        return json_decode($data,true);
    }

    function readString($name)
    {
        $p = $this->m_files[$name];
        return $p;
    }

    /**
     * 存储类型,io,FastDFS,
     */
    public static function storageType(){
        $cr = new ConfigReader();
        return $cr->readString("Storage")["type"];
    }

    /**
     * 存储加密
     */
    public static function storageEncrypt()
    {
        $cr = new ConfigReader();
        $sto = $cr->readString("Storage")["encrypt"];
        return $sto;
    }

    public static function blockWriter()
    {
        //if(StringTool::iequals(ConfigReader::storageType(), "FastDFS")) return new FastDFSWriter();
        //else if(StringTool::iequals(ConfigReader::storageType(), "minio")) return new MinioWriter();
        //else if(StringTool::iequals(ConfigReader::storageType(), "oss")) return new OSSWriter();
        return new FileBlockWriter();
    }

    public static function blockReader()
    {
        $st = ConfigReader::storageType();
        //if( 0 == strcasecmp($st,"FastDFS") ) return new FastDFSReader();
        //else if( 0 == strcasecmp($st,"minio") ) return new MinioReader();
        //else if( 0 == strcasecmp($st,"oss") ) return new OSSReader();
        return new FileBlockReader();
    }
}
?>