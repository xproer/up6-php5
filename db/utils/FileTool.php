<?php

class FileTool
{
    public static function readAll($path)
    {
        if(filesize($path)==0) return "";
        $handle = fopen($path, "r");
        $data = fread($handle, filesize ($path));
        fclose($handle);
        return $data;
    }
    
    function readBytes($path)
    {        
        $handle = fopen($path, "rb");
        $data = fread($handle, filesize ($path));
        fclose($handle);
        return $data;
    }

    public static function writeAll($path,$str)
    {
        $ret = true;
        $h = fopen($path,"wb");
        if(!fwrite($h,$str,strlen($str))) $ret=false;
        fclose($h);
        return $ret;
    }

    /**
     * 添加一行
     */
    public static function appendLine($path,$str)
    {
        $h = fopen($path,"ab");
        fwrite($h,$str,strlen($str));
        fwrite($h,"\n",1);
        fclose($h);
    }
}
?>