<?php

class StringTool
{   
    /*
     * 
     */
    public static function equals($a,$b)
    {
        return 0 == strcmp($a, $b);
    }
    
    /**
     * 比较两个字符串，忽略大小写
     * @param unknown $a
     * @param unknown $b
     * @return boolean
     */
    public static function iequals($a,$b)
    {
        return 0 == strcasecmp($a, $b);
    }
	
	public static function startsWith($p1,$p2)
	{
		$p1 = trim($p1);
		$p2 = trim($p2);
		if(strlen($p1)==0 || strlen($p2)==0) return false;
		return substr($p1,0,1)==$p2;
	}
	
	public static function endsWith($a,$b)
	{
		$a = trim($a);
		$b = trim($b);
		if(strlen($a)==0||strlen($b)==0) return false;

	    return substr($a,-1,1)==$b;
	}
}
?>