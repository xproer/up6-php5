<?php

class OpenSSL_AES_Encrypt{
    const BLOCK_SIZE = 32;

    private $method;

    public function __construct($method = null){
        if($method == null){
            $method = "AES-128-CBC";
        }
        $this->method = $method;
    }

    public function encrypt($data, $key, $iv) {
        $encrypted = openssl_encrypt($data, $this->method, $key,0, $iv);

        return $encrypted;
    }

    public function decrypt($data, $key, $iv) {
        $data = openssl_decrypt($data, $this->method, $key,0, $iv);

        return $data;
    }
}

?>