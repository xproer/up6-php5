<?php

/**
 * 构建文件夹结构
 * 格式：
 * [
 *   {f_id,nameLoc,pathSvr,pathRel,lenSvr,sizeSvr}
 *   {f_id,nameLoc,pathSvr,pathRel,lenSvr,sizeSvr}
 * ]
 * @author zysoft
 *
 */
class FolderBuilder
{   
    function build($id)
    {
        $se = new SqlExec();
        $o = $se->read("up6_files", "*", array("f_id"=>$id));
        //子目录
        if($o == null)
        {
            $o = $se->read("up6_folders", "*", array("f_id"=>$id));
        }
        
        $pathRoot = $o["f_pathRel"];
        $index = strlen($pathRoot);
        
        //查询文件
        $where = "locate('$pathRoot/',f_pathRel)=1 and f_fdTask=0 and f_deleted=0";
        $files = $se->select("up6_files", "*",$where);
        $count = count($files);
        
        $arr = array();
        for($i=0;$i<$count;$i++)
        {
            $f = $files[$i];
            $pathRel = $f["f_pathRel"];
            $pathRel = substr($pathRel, $index);
            $f["pathRel"] = $pathRel;
            $f["nameLoc"] = $f["f_nameLoc"];
            $f["pathSvr"] = $f["f_pathSvr"];
            $f["lenSvr"]  = $f["f_lenSvr"];            
            $f["lenSvrSec"] = $f["f_lenLocSec"];
            $f["encrypt"] = $f["f_encrypt"];
            $f["encryptAgo"] = $f["f_encryptAgo"];
            $f["blockSize"] = $f["f_blockSize"];
            $f["blockSizeSec"] = $f["f_blockSizeSec"];            
            $arr[] = $f;
        }
        return $arr;
    }
}
?>