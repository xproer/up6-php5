<?php

class fd_scan
{
	var $db;
	var $cmd_fd_add = null;
	var $cmd_f_add = null;
	var $cmd_cover = null;
	var $con_utf8 = null;
	var $root = null;//根节点
	var $sys = null;
	
	function __construct() 
	{
	    $this->sys = new SysTool();
		$this->db = new DbHelper();
		$this->con_utf8 =& $this->db->GetConUtf8();
	}
	
	function GetAllFiles($parent,$root)
	{
	    $folder_gbk = $this->sys->to_gbk($parent->pathSvr);
	    $dir_handle=opendir($folder_gbk);
	    if($dir_handle)
		{
			while(($file=readdir($dir_handle))!==false)
			{
				if($file==='.' || $file==='..')
				{
					continue;
				}				
						
				$tmp = $this->sys->to_gbk($parent->pathSvr) . '/' . $file;
				if(is_dir($tmp))
				{
				    
					$fd = new FileInf();
					$pb = new PathBuilderUuid();
					$fd->id = $pb->guid();
					$fd->pid = $parent->id;
					$fd->uid = $parent->uid;
					$fd->pidRoot = $this->root->id;
					//$fd->nameSvr = iconv("gbk","utf-8",$file);
					$fd->nameSvr = $this->sys->to_utf8($file);
					$fd->nameLoc = $fd->nameSvr;
					$fd->pathSvr = $this->sys->to_utf8($this->sys->to_gbk($parent->pathSvr) . '/' . $file);
					$fd->pathSvr = str_replace("\\", "/", $fd->pathSvr);
					//$fd->pathRel = substr($fd->pathSvr, strlen($root) + 1);
					//$fd->pathRel = PathTool::combin($parent->pathRel, $fd->pathRel);
					$fd->pathRel = PathTool::combin($parent->pathRel, $fd->nameLoc);
					$fd->perSvr = "100%";
					$fd->complete = true;
					$this->save_folder($fd);
									
					$this->GetAllFiles($fd, $root);
					
				} //文件
				else
				{
					$fl = new FileInf();
					$pb = new PathBuilderUuid();
					$fl->id = $pb->guid();
					$fl->pid = $parent->id;
					$fl->uid = $parent->uid;
					$fl->pidRoot = $this->root->id;
					//$fl->nameSvr = iconv("gbk","utf-8",$file);
					$fl->nameSvr = $this->sys->to_utf8($file);
					$fl->nameLoc = $fl->nameSvr;
					$fl->pathSvr = $this->sys->to_utf8($this->sys->to_gbk($parent->pathSvr) . '/' . $file);
					$fl->pathSvr = str_replace("\\","/", $fl->pathSvr);
					//$fl->pathRel = substr($fl->pathSvr, strlen($root) + 1);
					//$fl->pathRel = PathTool::combin($parent->pathRel, $fl->pathRel);
					$fl->pathRel = PathTool::combin($parent->pathRel, $fl->nameLoc);
					$fl->lenSvr = filesize( $this->sys->to_gbk($fl->pathSvr) );
					$fl->lenLoc = $fl->lenSvr;
					$fl->sizeLoc = PathTool::BytesToString($fl->lenSvr);
					$fl->perSvr = "100%";
					$fl->complete = true;
					$this->save_file($fl);
				}
			}
			closedir($dir_handle);
		}		
	}
	
	function getCoverFiles($parent,$root)
	{
	    $files = array();
	    $folder_gbk = $this->sys->to_gbk($parent->pathSvr);
	    $dir_handle=opendir($folder_gbk);
	    if($dir_handle)
	    {
	        while(($file=readdir($dir_handle))!==false)
	        {
	            if($file==='.' || $file==='..')
	            {
	                continue;
	            }
	
	            $tmp = $this->sys->to_gbk($parent->pathSvr) . '/' . $file;
	            if(is_dir($tmp))
	            {
	
	                $fd = new FileInf();
	                //$pb = new PathBuilderUuid();
	                //$fd->id = $pb->guid();
	                //$fd->pid = $parent->id;
	                //$fd->pidRoot = $this->root->id;
	                //$fd->nameSvr = iconv("gbk","utf-8",$file);
	                $fd->nameSvr = $this->sys->to_utf8($file);
	                $fd->nameLoc = $fd->nameSvr;
	                $fd->pathSvr = $this->sys->to_utf8($this->sys->to_gbk($parent->pathSvr) . '/' . $file);
	                $fd->pathSvr = str_replace("\\", "/", $fd->pathSvr);
	                //$fd->pathRel = substr($fd->pathSvr, strlen($root) + 1);
	                //$fd->pathRel = PathTool::combin($parent->pathRel, $fd->pathRel);
	                $fd->pathRel = PathTool::combin($parent->pathRel, $fd->nameLoc);
	                //$fd->perSvr = "100%";
	                //$fd->complete = true;
	                	
	                $this->getCoverFiles($fd, $root);
	                	
	            } //文件
	            else
	            {
	                $fl = new FileInf();
	                //$pb = new PathBuilderUuid();
	                //$fl->id = $pb->guid();
	                //$fl->pid = $parent->id;
	                //$fl->pidRoot = $this->root->id;
	                //$fl->nameSvr = iconv("gbk","utf-8",$file);
	                $fl->nameSvr = $this->sys->to_utf8($file);
	                $fl->nameLoc = $fl->nameSvr;
	                $fl->pathSvr = $this->sys->to_utf8($this->sys->to_gbk($parent->pathSvr) . '/' . $file);
	                $fl->pathSvr = str_replace("\\","/", $fl->pathSvr);
	                //$fl->pathRel = substr($fl->pathSvr, strlen($root) + 1);
	                //$fl->pathRel = PathTool::combin($parent->pathRel, $fl->pathRel);
	                $fl->pathRel = PathTool::combin($parent->pathRel, $fl->nameLoc);
	                //$fl->lenSvr = filesize( $this->sys->to_gbk($fl->pathSvr) );
	                //$fl->lenLoc = $fl->lenSvr;
	                //$fl->sizeLoc = PathTool::BytesToString($fl->lenSvr);
	                //$fl->perSvr = "100%";
	                //$fl->complete = true;
	                $files[] = $fl->pathRel;
	            }
	        }
	        closedir($dir_handle);
	    }
	    return $files;
	}
	
	function save_file($inf/*FileInf*/)
	{
		if(empty($this->cmd_f_add))
		{
			//bug:prepare中如果有返回值，则再次执行会报错。无论是否取完都无法再次执行。
			$sql = "
					insert into up6_files(
					 f_id
					,f_pid
					,f_pidRoot
					,f_fdTask
					,f_fdChild
					,f_uid
					,f_nameLoc
					,f_nameSvr
					,f_pathLoc
					,f_pathSvr
					,f_pathRel
					,f_md5
					,f_lenLoc
					,f_sizeLoc
					,f_lenSvr
					,f_perSvr
					,f_complete
					)
					values(
					 :id
					,:pid
					,:pidRoot
					,:fdTask
					,:fdChild
					,:uid
					,:nameLoc
					,:nameSvr
					,:pathLoc
					,:pathSvr
					,:pathRel
					,:md5
					,:lenLoc
					,:sizeLoc
					,:lenSvr
					,:perSvr
					,:complete
					)
					";
			$con = $this->con_utf8;
			$cmd = $con->prepare($sql);
			$this->cmd_f_add = $cmd;
		}
		$cmd = $this->cmd_f_add;
		$cmd->bindValue(":id", $inf->id );
		$cmd->bindValue(":pid", $inf->pid );
		$cmd->bindValue(":pidRoot", $inf->pidRoot );
		$cmd->bindValue(":fdTask", $inf->fdTask,\PDO::PARAM_BOOL);		
		$cmd->bindValue(":fdChild", true);//是文件夹中的文件
		$cmd->bindValue(":uid", $inf->uid);
		$cmd->bindValue(":nameLoc", $inf->nameLoc);
		$cmd->bindValue(":nameSvr", $inf->nameSvr);
		$cmd->bindValue(":pathLoc", $inf->pathLoc);
		$cmd->bindValue(":pathSvr", $inf->pathSvr);
		$cmd->bindValue(":pathRel", $inf->pathRel);
		$cmd->bindValue(":md5", $inf->md5);
		$cmd->bindValue(":lenLoc", $inf->lenLoc);
		$cmd->bindValue(":sizeLoc", $inf->sizeLoc);
		$cmd->bindValue(":lenSvr", $inf->lenSvr);
		$cmd->bindValue(":perSvr", $inf->perSvr);
		$cmd->bindValue(":complete", $inf->complete,\PDO::PARAM_BOOL);
		
		if(!$cmd->execute())
		{
			print_r($cmd->errorInfo());
		}
	}
	
	function save_folder($inf/*FileInf*/)
	{

		if(empty($this->cmd_fd_add))
		{
			//bug:prepare中如果有返回值，则再次执行会报错。无论是否取完都无法再次执行。
			$sql = "
					insert into up6_folders(
					 f_id
					,f_pid
					,f_pidRoot
					,f_uid
					,f_nameLoc
					,f_pathLoc
					,f_pathSvr
					,f_pathRel
					,f_complete
					)
					values(
					 :id
					,:pid
					,:pidRoot
					,:uid
					,:name
					,:pathLoc
					,:pathSvr
					,:pathRel
					,:complete
					)
					";
			$con = $this->con_utf8;
			$cmd = $con->prepare($sql);
			$this->cmd_fd_add = $cmd;
		}
		$cmd = $this->cmd_fd_add;
		$cmd->bindValue(":id", $inf->id );
		$cmd->bindValue(":pid", $inf->pid );
		$cmd->bindValue(":pidRoot", $inf->pidRoot);
		$cmd->bindValue(":uid", $inf->uid);//是文件夹中的文件
		$cmd->bindValue(":name", $inf->nameSvr);
		$cmd->bindValue(":pathLoc", $inf->pathLoc);
		$cmd->bindValue(":pathSvr", $inf->pathSvr);
		$cmd->bindValue(":pathRel", $inf->pathRel );
		$cmd->bindValue(":complete", $inf->complete,\PDO::PARAM_BOOL);
		
		if(!$cmd->execute())
		{
			print_r($cmd->errorInfo());
		}
	}
	
	function cover_file($pathRel)
	{
	    if(empty($this->cmd_cover))
	    {
	        //bug:prepare中如果有返回值，则再次执行会报错。无论是否取完都无法再次执行。
	        $sql = "update up6_files set f_deleted=1 where f_pathRel=:f_pathRel";
	        $con = $this->con_utf8;
	        $cmd = $con->prepare($sql);
	        $this->cmd_cover = $cmd;
	    }
	    $cmd = $this->cmd_cover;
	    $cmd->bindValue(":f_pathRel", $pathRel);
	
	    if(!$cmd->execute())
	    {
	        print_r($cmd->errorInfo());
	    }
	}
	
	function cover($inf, $pathParent)
	{
	    $files = $this->getCoverFiles($inf, $pathParent);
	    foreach($files as $f)
	    {
	        $this->cover_file($f);
	    }
	}

	function scan($inf, $root)
	{
		$this->GetAllFiles($inf, $root);
	}	
}
?>