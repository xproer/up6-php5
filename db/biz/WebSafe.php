<?php

class WebSafe 
{
	function validToken($token,$f,$action="init")
	{
	    $cr = new ConfigReader();
	    $encrypt = $cr->readString("security")["token"];
	    if($encrypt)
	    {
	        if(empty(trim($token))) return false;
	        $ct = new CryptoTool();
	        return $ct->token($f,$action) == $token;
	    }
	    return true;
	}
}

?>