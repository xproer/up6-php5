<?php

class FolderSchema
{
	
	function __construct() 
	{
	}

    /// <summary>
    /// 创建结构文件
    /// pathSvr/scheme.txt
    /// </summary>
    /// <param name="dir"></param>
    function create(/*FileInf*/ $dir)
    {
        $file = $dir->schemaFile();
        $val = $dir->toJson();//不编码/，不编码汉字
        return FileTool::writeAll($file,$val."\n");
    }

    /// <summary>
    /// 添加一条记录
    /// </summary>
    /// <param name="f"></param>
    function addFile(/*FileInf*/ $f)
    {
        $root = "";
        if ($f->pathRel != "") $root = str_replace($f->pathRel,"",$f->pathSvr);
        
        //guid/Soft/folder1/QQ.exe->/guid/
        $root = dirname($root);
        //guid/Soft/folder1/QQ.exe->/guid/schema.txt
        $file = $root . "/schema.txt";
        $val = $f->toJson();//不编码/，不编码汉字

        if (file_exists($file)) FileTool::appendLine($file, $val);
    }
		
}
?>