<?php

/**
 * 相对路径构建器
 * @author zysoft
 *
 */
class PathRelBuilder
{
    var $m_folders = null;//目录列表
    var $m_files = array();
    var $m_fdQuerys = array();//要查询的子目录列表
    var $m_childs = array();//子目录列表
    
    function build($id,$pidRoot)
    {
        $this->query($id, $pidRoot);
        $this->buildFolder($id);
        return $this->buildFile();
    }
    
    function query($id,$pidRoot)
    {        
        //查询目录
        $se = new SqlExec();
        $folders = $se->select("up6_folders"
            , "f_id,f_nameLoc,f_pid,f_pidRoot"
            , array("f_deleted"=> 0)
            );
       
        $bizFd = new BizFolder();
        $this->m_folders = $bizFd->toDic($folders);
       
        //根节点
        if($id == $pidRoot)
        {
            $root = $se->read("up6_files", "f_nameLoc", array("f_id"=>$id));
            $this->m_folders[$id] = array("f_id"=>$id,"f_pid"=>"","f_pidRoot"=>"","f_nameLoc"=>$root["f_nameLoc"]);
        }
       
        //查询文件
        $this->m_files = $se->select("up6_files"
            , "f_id,f_pid,f_nameLoc,f_pathSvr,f_pathRel,f_lenSvr,f_sizeLoc"
            , array("f_pidRoot"=>$pidRoot,"f_deleted"=>false,"f_complete"=>true));
    }
    
    function buildFolder($id)
    {
        $idCur = $id;
        $this->m_childs[$id] = $id;
        $fdPrev = $this->m_folders[$id];
        $fdPrev["f_pathRel"] = "";
        $this->m_folders[$id]["f_pathRel"] = "";
        
        while(true)
        {
            $this->updateChilds($idCur, $fdPrev["f_pathRel"]);
            if( count($this->m_fdQuerys) == 0 ) return;
            
            $idCur = $this->m_fdQuerys[0];            
            array_splice($this->m_fdQuerys, 0, 1);
            
            $fdCur = $this->m_folders[$idCur];
            $pid = $fdCur["f_pid"];
            $fdParent = $this->m_folders[$pid];
            $fdCur["f_pathRel"] = $fdParent["f_pathRel"] . "/" . $fdCur["f_nameLoc"];
            
            //清除无关数据
            foreach($this->m_childs as $c)
            {
                if( !key_exists($c, $this->m_folders) ) array_diff_key($this->m_folders,array($c));
            }
            
        }
    }
    
    function buildFile()
    {
        $bizFd = new BizFolder();
        $fs = $bizFd->toDic($this->m_files);
        
        $arr = array();
        foreach($fs as $k=>$f)
        {
            if( key_exists($k, $this->m_childs) ) continue;
            
            $parent = $this->m_folders[$f["f_pid"]];
            $f["f_pathRel"] = $parent["f_pathRel"] . "/" . $f["f_nameLoc"];
            
            $f["nameLoc"] = $f["f_nameLoc"];
            $f["pathSvr"] = $f["f_pathSvr"];
            $f["pathRel"] = $f["f_pathRel"];
            $f["lenSvr"] = $f["f_lenSvr"];
            $f["sizeSvr"] = $f["f_sizeLoc"];
            $arr[] = $f;
        }
        return $arr;
    }
    
    function updateChilds($pid,$parentRel)
    {       
        $childs = array();
        foreach($this->m_folders as $k=>$v)
        {            
            if( strcmp($v["f_pid"],$pid)==0 ) $childs[$k] = $v;
        }
        
        foreach($childs as $k => $v)
        {
            $this->m_fdQuerys[] = $k;
            $this->m_childs[ $k ] = $k;
            $this->m_folders[$k]["f_pathRel"] = $parentRel . "/" . $v["f_nameLoc"];
            if( empty($parentRel) )
            {
                $this->m_folders[$k]["f_pathRel"] = $v["f_nameLoc"];
            }
        }
               
        return count($childs)>0;
    }
}
?>