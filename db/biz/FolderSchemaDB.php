<?php

class FolderSchemaDB
{
	var $m_root;/**FileInf */
    var $m_files;/**文件列表 ,FileInf*/
    var $m_folders;/**目录列表 ,FileInf*/
    var $m_dirs;/**目录map表 */

	function __construct() 
	{
	}

    /// <summary>
    /// 解析文件表中的目录信息
    /// </summary>
    /// <param name="dir"></param>
    function parseParent()
    {
        foreach ($this->m_files as $file)
        {
            $dir = new FileInf();
            $dir->id = $file->pid;
            $dir->uid = $this->m_root->uid;
            $dir->pidRoot = $file->pidRoot;
            $dir->pathSvr = dirname($file->pathSvr);
            $dir->pathRel = str_replace($this->m_root->pathSvr,"",$dir->pathSvr);
            $dir->nameLoc = PathTool::getName($dir->pathSvr);//取目录名称
            $dir->nameSvr = $dir->nameLoc;
            $dir->fdChild = true;
            $dir->complete = true;

            //不存在->添加
            if (!key_exists($dir->pathSvr,$this->m_dirs) )
            {
                $this->m_folders[] = $dir;
                $this->m_dirs[$dir->pathSvr] = $dir;
            }
        }
    }

    /// <summary>
    /// 分析目录列表中的子目录
    /// </summary>
    /// <param name="f"></param>
    function parseDirs()
    {
        foreach($this->m_folders as $file)
        {
            $dir = $file->pathSvr;
            //依次取子目录并保存
            $pos = strpos(strlen($this->m_root->pathSvr),"/");
            while($pos)
            {
                $fd = new FileInf();
                $fd->uid = $this->m_root->uid;
                $fd->pathSvr = substr($dir,0,$pos);//D:\\Soft\Network\QQ.exe => D:\\Soft
                $fd->pidRoot = $this->m_root->id;
                $fd->nameLoc = PathTool::getName($fd->pathSvr);
                $fd->nameSvr = $fd->nameLoc;
                $fd->pathRel = str_replace($fd->pathSvr,"",$this->m_root->pathSvr);
                $fd->complete = true;
                $fd->fdChild = true;

                //目录不存在
                if( !key_exists($fd->pathSvr,$this->m_dirs))
                {
                    $fd->id = new_guid();
                    $this->m_folders[] = $fd;
                    $this->m_dirs[$fd->pathSvr] = $fd;
                }
                $pos = strpos("/",$pos+1);
            }
        }
    }

    /**
     * 更新所有子目录，子文件PID
     */
    function updatePID(){
        //更新子目录pid,第一条为根目录，忽略掉。
        for($i = 1 ,$l=count($this->m_folders);$i<$l;++$i)
        {
            $fd = $this->m_folders[$i];
            $dir = $fd->parentDir();
            $pfd = $this->m_dirs[$dir];
            $fd->pid = $pfd->id;
        }

        //更新子文件PID
        foreach($this->m_files as $f)
        {
            $dir = $f->parentDir();
            $fd = $this->m_dirs[$dir];
            $f->pid = $fd->id;
        }
    }

    /**
     * 更新文件相对路径,提供给文件管理器使用
     *   1.增加根目录前缀
     */
    function updatePathRel(){
        foreach($this->m_folders as $fd)
        {
            //更新相对路径->增加根目录前缀
            if($fd->pid != "")
            {
                $fd->pathRel = PathTool::combin($this->m_root->pathRel,$fd->pathRel);
            }
        }
        foreach($this->m_files as $f)
        {
            $f->pathRel = PathTool::combin($this->m_root->pathRel,$f->pathRel);
        }
    }

    function save(/*FileInf*/$dir){
        $this->m_root = $dir;
        $this->m_files = array();
        $this->m_folders = array();
        $this->m_dirs = array();

        //解析结构文件.scheme.txt
        $lines = file($dir->schemaFile());
        foreach($lines as $line)
        {
            $file = new FileInf();
            $file->parseSchema($line);
            $file->uid = $this->m_root->uid;
            if($file->fdTask)
            {
                $this->m_folders[] = $file;
                $this->m_dirs[$file->pathSvr] = $file;
            }
            else
            {
                $file->sizeLoc = $file->formatSize($file->lenLoc);
                $this->m_files[] = $file;
            }
        }

        //分析
        $this->parseParent();
        $this->parseDirs();
        $this->updatePID();
        $this->updatePathRel();

        //删除根目录，根目录已经添加到数据库
        array_splice($this->m_folders,0,1);

        //添加到目录表
        $fd = new DBFolder();
        $fd->addBatch($this->m_folders);

        //添加到文件表
        $df = new DBFile();
        $df->addBatch($this->m_files);
    }

    function cover_files(){
        $db = new DbHelper();
        $cmd = $db->prepare_utf8("update up6_files set f_deleted=1 where f_pathRel=:pathRel");
        foreach($this->m_files as $f)
        {
            $cmd->bindParam(":pathRel", $f->pathRel);
            $db->ExecuteRow($cmd);
        }
    }
    function cover_folders(){
        
        $db = new DbHelper();
        $cmd = $db->prepare_utf8("update up6_folders set f_deleted=1 where f_pathRel=:pathRel");
        foreach($this->m_folders as $f)
        {
            $cmd->bindParam(":pathRel", $f->pathRel);
            $db->ExecuteRow($cmd);
        }
    }
    function cover(){
        $this->cover_files();
        $this->cover_folders();
    }
}
?>