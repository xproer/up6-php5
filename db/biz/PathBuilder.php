<?php

/*
	路径生成器
	提供文件或文件夹的存储路径
	更新记录：
		2016-04-12 创建
*/
class PathBuilder
{	

	function __construct() 
	{
	}
	
	/*
		返回上传路径：
		示例：D:\wamp\www\HttpUploader6.1\upload\
	*/
	function getRoot()
	{		
        //定位到/config/
        $root = str_replace("\\", "/", __FILE__);
        $pos = strpos($root,"/db/biz/PathBuilder.php");        
        //
        $root = substr($root, 0,$pos+1);

		$cr = new ConfigReader();
		$pathSvr = $cr->readString("IO")["dir"];
		$pathSvr = str_replace("{root}",$root,$pathSvr);
		$pathSvr = str_replace("\\","/",$pathSvr);
		return $pathSvr;
	}
	
	function genFolder(&$fd/*FileInf*/){}
	function genFile($uid,&$f/*FileInf*/){}
	/*
	 相对路径转换成绝对路径
	格式：/2021/05/28/guid/nameLoc => d:/upload/2021/05/28/guid/nameLoc
	*/
	function relToAbs($path)
	{
		$root = $this->getRoot();
		$root = str_replace("\\","/",$root);
		$path = str_replace("\\","/",$path);
		if(false===strpos( $path,$this->getRoot()) )
		{
			$path = PathTool::combin($root, $path);
		}
		
		return $path;
	}
	/*
	 将路径转换成相对路径
	 a/b/c/d/e
	*/
	function absToRel($path)
	{
		$root = str_replace("\\","/",$this->getRoot());
		$path = str_replace("\\","/",$path);
		$path = str_replace($root,"",$path);
		if(StringTool::startsWith($path,"/")) return substr($path,1);
		return $path;
	}
}
?>