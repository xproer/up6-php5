<?php

class BizFolder
{
    var $root = array("f_id"=>"","f_nameLoc"=>"根目录","f_pid"=>"","f_pidRoot"=>"","f_pathRel"=>"");
    
    function __construct()
    {
    }
    
    /**
     * 将json索引数组转换成关联数组
     * @param array $folders 索引数组
     * @return array()
     */
    function toDic($folders,$key="f_id")
    {
        $dt = array();
        for($i=0,$l=count($folders);$i<$l;++$i)
        {
            $f = $folders[$i];
            $dt[ $f[$key] ] = $f;
        }
        return $dt;
    }
    
    function foldersToDic($pidRoot)
    {
        //默认加载根目录
        $sql = "select f_id,f_nameLoc,f_pid,f_pidRoot from up6_folders where f_pidRoot='$pidRoot'";
        
        $se = new SqlExec();
        $folders = $se->exec_arr("up6_folders", $sql, "f_id,f_nameLoc,f_pid,f_pidRoot","");
        return $this->toDic($folders);
    }
    
    /**
     * 
     * @param array $dt 关联数组
     * @param string $idCur 
     * @param array $psort 数组
     */
    function sortByPid($dt, $idCur, $psort) 
    {
        
        $cur = $idCur;
        while (true)
        {
            //key不存在
            if ( !array_key_exists($cur,$dt) ) break;
            
            $d = $dt[$cur];//查父ID
            array_unshift($psort, $d);
            $cur = trim($d["f_pid"]);//取父级ID
            
            if ($cur == "0") break;
            if (empty($cur)) break;
        }
        return $psort;
    }
    
    function build_path_by_id($fdCur) 
    {
        
        $id = trim($fdCur["f_id"]);//
        $pidRoot = trim($fdCur["f_pidRoot"]);//
        
        //根目录
        $psort = array();
        if (empty($id))
        {
            $psort[]=$this->root;
            
            return json_encode($psort,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
        }
        
        //当前目录是子目录
        $se = new SqlExec();
        $sql = "(select f_pidRoot from up6_files where f_id='$id') union (select f_pidRoot from up6_folders where f_id='$id')";
        $data = $se->exec_arr("up6_files", $sql, "f_pidRoot","");        
        if( count($data) > 0) $pidRoot = trim($data[0]["f_pidRoot"]);
        
        //构建目录映射表(id,folder)
        $dt = $this->foldersToDic($pidRoot);
        
        //按层级顺序排列目录
        $psort = $this->sortByPid($dt, $id, $psort);
        
        
        //是子目录->添加根目录
        if (!empty($pidRoot))
        {
            $cur = $se->read("up6_files", "f_id,f_nameLoc,f_pid,f_pidRoot", array("f_id"=>$pidRoot) );
            
            array_unshift($psort,$cur);
        }//是根目录->添加根目录
        else if (!empty($id) && empty($pidRoot))
        {
            $root = $se->read("up6_files", "f_id,f_nameLoc,f_pid,f_pidRoot", array("f_id"=>$id) );
            array_unshift($psort,$root);
        }
        array_unshift($psort,$this->root);
        
        return json_encode($psort,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
    }
    
    function build_path($fdCur)
    {
        //查询文件表目录数据
        $se = new SqlExec();
        $files = $se->select("up6_files", "f_id,f_pid,f_nameLoc,f_pathRel",array("f_fdTask"=>true) );
        $folders = $se->select("up6_folders", "f_id,f_pid,f_nameLoc,f_pathRel",array() );
        $id = trim($fdCur["f_id"]);//
        
        //根目录
        $psort = array();
        if (empty($id))
        {
            $psort[]=$this->root;
    
            return json_encode($psort,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
        }
        
        //构建目录映射表(id,folder)
        $dtFiles = $this->toDic($files);
        $dtFolders = $this->toDic($folders);
    
        foreach($dtFolders as $k=>$f)
        {
            if( !key_exists($k, $dtFiles) ) 
            {
            	$dtFiles[$k] = $f;
            }
        }        
        
        
        //按层级顺序排列目录
        $psort = $this->sortByPid($dtFiles, $id, $psort);
    

        array_unshift($psort,$this->root);
    
        return json_encode($psort,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
    }
    
    function exist_same_file($name,$pid)
    {
        $swm = new SqlWhereMerge();
        $swm->equal("f_nameLoc", $name);
        $swm->equal("f_pid", $pid);
        $swm->equal("f_deleted", 0);
        
        $sql = "select f_id from up6_files where " . $swm->to_sql();
        
        $se = new SqlExec();
        $arr = $se->exec_arr("up6_files", $sql, "f_id", "");
        return count($arr) > 0;
    }
    
    function exist_same_folder($name,$pid)
    {
        $swm = new SqlWhereMerge();
        $swm->equal("f_nameLoc", $name );
        $swm->equal("f_deleted", 0);
        $swm->equal("LTRIM (f_pid)", $pid );
        $where = $swm->to_sql();
        
        $sql = ("(select f_id from up6_files where $where ) union (select f_id from up6_folders where $where)");
        
        $se = new SqlExec();
        $fids = $se->exec_arr("up6_files", $sql, "f_id", "");
        return count($fids) > 0;
    }
    
    /**
     * 读取根目录或子目录信息
     * @param string $id
     * @return array 关联数组
     */
    function read($id) {
        $se = new SqlExec();
        $sql = ("(select f_pid,f_pidRoot,f_pathSvr,f_pathRel from up6_files where f_id='$id') union (select f_pid,f_pidRoot,f_pathSvr,f_pathRel from up6_folders where f_id='$id')");
        $data = $se->exec_arr("up6_files", $sql, "f_pid,f_pidRoot,f_pathSvr,f_pathRel","");
        $o = $data[0];
                
        $file = new FileInf();
        $file->id = $id;
        $file->pid = trim($o["f_pid"]);
        $file->pidRoot = trim($o["f_pidRoot"]);
        $file->pathSvr = trim($o["f_pathSvr"]);
        $file->pathRel = trim($o["f_pathRel"]);
        return $file;
    }
    
    /**
     * 取同名目录信息
     * @param string $pathRel
     * @param string $pid
     * @return
     */
    function readCover($pathRel, $pid, $id) {
        $se = new SqlExec();
        $sql = ("(select f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel from up6_files where f_pid='$pid' and f_pathRel='$pathRel' and f_deleted=0 and f_id!='$id') union (select f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel from up6_folders where f_pid='$pid' and f_pathRel='$pathRel' and f_deleted=0 and f_id!='$id')");
        $data = $se->exec_arr("up6_files", $sql, "f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel","");
        if(count($data) < 1) return null;
        
        $o = $data[0];
    
        $file = new FileInf();
        $file->id = trim($o["f_id"]);
        $file->pid = trim($o["f_pid"]);
        $file->pidRoot = trim($o["f_pidRoot"]);
        $file->pathSvr = trim($o["f_pathSvr"]);
        $file->pathRel = trim($o["f_pathRel"]);
        return $file;
    }
    
    /// <summary>
    /// 重命名文件检查
    /// </summary>
    /// <param name="newName"></param>
    /// <param name="pid"></param>
    /// <returns></returns>
    function rename_file_check($newName,$pid)
    {
        $se = new SqlExec();
        $res = $se->select("up6_files", "f_id",array("f_nameLoc"=>$newName,"f_pid"=>$pid) );
        return count($res) > 0;
    }
    
    /// <summary>
    /// 重命名目录检查
    /// </summary>
    /// <param name="newName"></param>
    /// <param name="pid"></param>
    /// <returns></returns>
    function rename_folder_check($newName, $pid)
    {
        $se = new SqlExec();
        $res = $se->select("up6_folders", "f_id", array("f_nameLoc"=>$newName,"f_pid"=>$pid) );
        return count($res) > 0;
    }
    
    function rename_file($name,$id) 
    {
        $se = new SqlExec();
        $se->update2("up6_files",array("f_nameLoc"=>$name),array("f_id"=>$id) );
    }
    
    function rename_folder($name, $id, $pid) 
    {
        $se = new SqlExec();
        $se->update2("up6_folders", array("f_nameLoc"=>$name),array("f_id"=>$id ));
    }
    
    static function del($id, $uid)
    {
        $se = new SqlExec();
        $se->update2("up6_files", array("f_deleted"=>true),array("f_id"=>$id,"f_uid"=>$uid ));
        $se->update2("up6_folders", array("f_deleted"=>true),array("f_id"=>$id,"f_uid"=>$uid ));
    }
    
    /**
     * 添加子目录
     */
    function addChildFolder($fd)
    {
        $se = new SqlExec();
        $se->insert("up6_folders"
            , array(
                 "f_id"=>$fd->id
                ,"f_pid"=>$fd->pid
                ,"f_pidRoot"=>$fd->pidRoot
                ,"f_uid"=>$fd->uid
                ,"f_nameLoc"=>$fd->nameLoc
                ,"f_sizeLoc"=>$fd->sizeLoc
                ,"f_pathLoc"=>$fd->pathLoc
                ,"f_pathSvr"=>$fd->pathSvr
                ,"f_pathRel"=>$fd->pathRel
                ,"f_complete"=>$fd->complete
                ,"f_deleted"=>0
            ));
    }
}
?>