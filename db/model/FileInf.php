<?php
/*
	更新记录：
		2014-08-12 更新
		2017-07-12 更名为FileInf
*/
class FileInf
{
	var $id="";
	/**
	 * 文件夹ID
	 */
	var $pid="";
    /**
     * 根级文件夹ID
     */
    var $pidRoot="";
	/**
	 * 表示当前项是否是一个文件夹项。
	 */
	var $fdTask = false;
	/// <summary>
	/// 是否是文件夹中的子文件
	/// </summary>
	var $fdChild = false;
	/**
	 * 用户ID。与第三方系统整合使用。
	 */
	var $uid="0";
	/**
	 * 文件在本地电脑中的名称
	 */
	var $nameLoc="";
	/**
	 * 文件在服务器中的名称。
	 */
	var $nameSvr="";
	/**
	 * 文件在本地电脑中的完整路径。示例：D:\Soft\QQ2012.exe
	 */
	var $pathLoc="";
	/**
	 * 文件在服务器中的完整路径。示例：F:\\ftp\\uer\\md5.exe
	 */
	var $pathSvr="";
	/**
	 * 文件在服务器中的相对路径。示例：/www/web/upload/md5.exe
	 */
	var $pathRel="";
	/**
	 * 文件MD5
	 */
	var $md5="";
	/**
	 * 数字化的文件长度。以字节为单位，示例：120125
	 */
	var $lenLoc=0;
	/**
	 * 文件加密大小
	 * @var integer
	 */
	var $lenLocSec=0;
	/**
	 * 格式化的文件尺寸。示例：10.03MB
	 */
	var $sizeLoc="";
	/**
	 * 文件续传位置。相对于整个文件的偏移
	 */
	var $offset=0;
	/**
	 * 已上传大小。以字节为单位
	 */
	var $lenSvr=0;
	/**
	 * 已上传百分比。示例：10%
	 */
	var $perSvr="0%";
	var $complete=false;
	var $PostedTime;
	var $deleted=false;
	var $scaned=false;
	
	/// <summary>
	/// 块索引，基于1
	/// </summary>
	var $blockIndex = 0;
	/// <summary>
	/// 块偏移，基于整个文件
	/// </summary>
	var $blockOffset = 0;
	/// <summary>
	/// 块加密偏移
	/// </summary>
	var $blockOffsetCry = 0;
	/// <summary>
	/// 块总数
	/// </summary>
	var $blockCount = 0;
	/// <summary>
	/// 块大小
	/// </summary>
	var $blockSize = 0;
	/// <summary>
	/// 块加密大小
	/// </summary>
	var $blockSizeSec = 0;
	/// <summary>
	/// 块压缩大小
	/// </summary>
	var $blockSizeCpr = 0;
	/// <summary>
	/// 块压缩算法,gzip,zip
	/// </summary>
	var $blockCprType = "";
	/// <summary>
	/// 块是否加密
	/// </summary>
	var $blockEncrypt = false;
	/**
	 * 块数据，临时文件路径
	 * @var string
	 */
	var $blockPath="";
	/// <summary>
	/// 用于第三方存储的对象ID，如FastDFS,Minio等
	/// </summary>
	var $object_id="";
	var $etag="";
	/// <summary>
	/// 文件是否加密
	/// </summary>
	var $encrypt = false;
	var $encryptAgo = "";
	
	function __construct()
	{
		date_default_timezone_set("PRC");//fix(2016-12-06):在部分server中提示警告
		$this->PostedTime = getdate();
	}

	/**
	 * 生成层级信息文件路径
	 */
	public function schemaFile()
	{
		return PathTool::combin($this->parentDir(),"schema.txt");
	}
	
	public function getObjectID(){
	    return $this->object_id;
	}

	public function parentDir()
	{
		return dirname($this->pathSvr);
	}

	public function formatSize($byteCount)
	{
	    return PathTool::BytesToString($byteCount);
	}
	
	/**
	 * 保存到层级信息文件
	 */
	public function saveScheme()
	{
		//仅第一块保存
		if (1!=$this->blockIndex ) return;
		//仅保存子文件数据
		if ( strlen($this->pid) == 0) return;
		
		$fs = new FolderSchema();
		$fs->addFile($this);
	}

	//解析
	public function parseSchema(/*string*/$line)
	{
		$v = json_decode($line,true);//to array map
		$this->id = $v["id"];
		$this->pid = $v["pid"];
		$this->pidRoot = $v["pidRoot"];
		$this->fdTask = $v["fdTask"];
		$this->fdChild = $v["fdChild"];
		$this->uid = $v["uid"];
		$this->nameLoc = $v["nameLoc"];
		$this->nameSvr = $v["nameSvr"];
		$this->pathLoc = $v["pathLoc"];
		$this->pathSvr = $v["pathSvr"];
		$this->pathRel = $v["pathRel"];
		$this->md5 = $v["md5"];
		$this->lenLoc = $v["lenLoc"];
		$this->lenLocSec = $v["lenLocSec"];
		$this->sizeLoc = $v["sizeLoc"];
		$this->offset = $v["offset"];
		$this->lenSvr = $v["lenSvr"];
		$this->perSvr = $v["perSvr"];
		$this->complete = $v["complete"];
		$this->PostedTime = $v["PostedTime"];
		$this->blockIndex = $v["blockIndex"];
		$this->blockCount = $v["blockCount"];
		$this->blockSize = $v["blockSize"];
		$this->blockSizeSec = $v["blockSizeSec"];
		$this->object_id = $v["object_id"];
		$this->etag = $v["etag"];
		$this->encrypt = $v["encrypt"];
		$this->encryptAgo = $v["encryptAgo"];
	}

	public function toJson(){
		//PHP5 JSON转换时字符串必须是UTF8编码
		return json_encode($this,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
	}
}
?>