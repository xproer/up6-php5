function Uploader6App(mgr) {
    var _this = this;
    this.ins = mgr;
    this.edgeApp = mgr.edgeApp;
    this.Config = mgr.Config;
    this.checkFF = function () {
        var mimetype = navigator.mimeTypes;
        if (typeof mimetype == "object" && mimetype.length) {
            for (var i = 0; i < mimetype.length; i++) {
                var enabled = mimetype[i].type == this.Config.firefox.type;
                if (!enabled) enabled = mimetype[i].type == this.Config.firefox.type.toLowerCase();
                if (enabled) return mimetype[i].enabledPlugin;
            }
        }
        else {
            mimetype = [this.Config.firefox.type];
        }
        if (mimetype) {
            return mimetype.enabledPlugin;
        }
        return false;
    };
    this.Setup = function () {
        //文件夹选择控件
        acx += '<object id="objHttpPartition" classid="clsid:' + this.Config.ie.part.clsid + '"';
        acx += ' codebase="' + this.Config.ie.path + '" width="1" height="1" ></object>';

        $("body").append(acx);
    };
    this.init = function () {
        var param = { name: "init", config: this.Config };
        this.postMessage(param);
    };
    this.initNat = function () {
        if (!this.chrome45) return;
        this.exitEvent();
        document.addEventListener('Uploader6EventCallBack', function (evt) {
            this.recvMessage(JSON.stringify(evt.detail));
        });
    };
    this.initEdge = function () {
        this.edgeApp.run();
    };
    this.exit = function () {
        var par = { name: 'exit' };
        this.postMessage(par);
    };
    this.exitEvent = function () {
        var obj = this;
        $(window).bind("beforeunload", function () { obj.exit(); });
    };
    this.addFile = function (json) {
        var param = { name: "add_file", config: this.Config };
        $.extend(param, json);
        this.postMessage(param);
    };
    this.addFolder = function (json) {
        var param = { name: "add_folder", config: this.Config };
        $.extend(param, json);
        this.postMessage(param);
    };
    this.openFiles = function () {
        var param = { name: "open_files", config: this.Config };
        this.postMessage(param);
    };
    this.openFolders = function () {
        var param = { name: "open_folders", config: this.Config };
        this.postMessage(param);
    };
    this.pasteFiles = function () {
        var param = { name: "paste_files", config: this.Config };
        this.postMessage(param);
    };
    this.checkFile = function (f) {
        var param = { name: "check_file", config: this.Config };
        $.extend(param, f);
        this.postMessage(param);
    };
    this.checkFolder = function (fd) {
        var param = { name: "check_folder", config: this.Config };
        $.extend(param, fd);
        param.name = "check_folder";
        this.postMessage(param);
    };
    this.scanFolder = function (fd) {
        var param = { name: "scan_folder" };
        $.extend(param, fd);
        this.postMessage(param);
    };
    this.checkFolderNat = function (fd) {
        var param = { name: "check_folder", config: this.Config, folder: JSON.stringify(fd) };
        this.postMessage(param);
    };
    this.postFile = function (f) {
        var param = { name: "post_file", config: this.Config };
        $.extend(param, f);
        this.postMessage(param);
    };
    this.postFolder = function (fd) {
        var param = { name: "post_folder", config: this.Config };
        $.extend(param, fd);
        param.name = "post_folder";
        this.postMessage(param);
    };
    this.updateFolder = function (fd) {
        var param = { name: "update_folder" };
        $.extend(param, fd);
        this.postMessage(param);
    };
    this.delFolder = function (v) {
        var param = { name: "del_folder" };
        $.extend(param, v);
        this.postMessage(param);
    };
    this.stopFile = function (f) {
        var param = $.extend({}, f, { name: "stop_file" });
        this.postMessage(param);
    };
    this.delFile = function (f) {
        var param = { name: "del_file", id: f.id };
        this.postMessage(param);
    };
    this.stopQueue = function () {
        var param = { name: "stop_queue" };
        this.postMessage(param);
    };
    this.postMessage = function (json) {
        try {
            this.ins.parter.postMessage(JSON.stringify(json));
        } catch (e) { }
    };
    this.postMessageNat = function (par) {
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(this.entID, true, false, par);
        document.dispatchEvent(evt);
    };
    this.postMessageEdge = function (par) {
        this.edgeApp.send(par);
    };
}