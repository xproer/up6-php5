<?php
require_once('../../db/head.php');

$head = new HttpHeader();

$id 		 = $head->param("id");
$blockIndex  = $head->paramInt("blockIndex");
$blockOffset = $head->paramInt("blockOffset");
$blockSize 	 = $head->paramInt("blockSize");
$pathSvr 	 = $head->param("pathSvr");
$pathSvr	 = PathTool::urldecode_path($pathSvr);


if ( empty($id) 
	|| empty($pathSvr) ) 
{
	$ps = array("id"=>$id,
	"blockIndex"=>$blockIndex,
	"blockOffset"=>$blockOffset,
	"blockSize"=>$blockSize,
	"pathSvr"=>$pathSvr);
	
	header('HTTP/1.1 500 param null,'.json_encode($ps));
	return;
}

header("Cache-Control: public");
header("Content-Type: application/octet-stream");
header("Content-Length: $blockSize");
header("Content-Range: $blockOffset-".($blockSize-1)."/$blockSize");
header("Content-Transfer-Encoding: binary");
header('Pragma: no-cache');
header('Expires: 0');

//windows系统中需要将中文转换为gb2312
$sys = new SysTool();
$pathSvr = $sys->to_gbk($pathSvr);

//文件不存在
if(!file_exists($pathSvr))
	die("file not found,pathSvr:$pathSvr");

$file = fopen($pathSvr,"rb");
fseek($file,$blockOffset);
$readToLen = $blockSize;
//清空缓存区
ob_clean();
while( $readToLen > 0)
{
	set_time_limit(0);
	$len = min(1048576,$readToLen);
	print( fread($file,$len ));
	$readToLen -= $len;
	flush();
	ob_flush();
}
fclose($file);
?>