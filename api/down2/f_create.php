<?php 
require_once('../../db/head.php');

$id 		= $_GET["id"];
$uid 		= $_GET["uid"];
$fdTask 	= $_GET["fdTask"];
$nameLoc 	= $_GET["nameLoc"];
$pathLoc 	= $_GET["pathLoc"];
$lenSvr 	= $_GET["lenSvr"];
$sizeSvr 	= $_GET["sizeSvr"];
$cbk 		= $_GET["callback"];
$pathLoc	= PathTool::urldecode_path($pathLoc);
$nameLoc	= PathTool::urldecode_path($nameLoc);
$sizeSvr	= PathTool::urldecode_path($sizeSvr);

if (  strlen($uid) < 1
	||empty($pathLoc)	
	||empty($lenSvr))
{
	echo $cbk . "({\"value\":null})";
	die();
}

$inf = new DnFileInf();
$inf->id = $id;
$inf->uid = $uid;
$inf->nameLoc = $nameLoc;
$inf->pathLoc = $pathLoc;
$inf->lenSvr = intval($lenSvr);
$inf->sizeSvr = $sizeSvr;
$inf->fdTask = StringTool::iequals($fdTask,"1");

$db = new DnFile();
$db->Add($inf);

$json = json_encode($inf,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
$json = urlencode($json);
$json = "$cbk({\"value\":\"".$json."\"})";//返回jsonp格式数据。
echo $json;
?>