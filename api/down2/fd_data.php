<?php
header('Content-Type: text/html;charset=utf-8');

require_once('../../db/head.php');

/*
 * 获取文件夹，子文件列表，返回JSON格式
	更新记录：
		2015-05-13 创建
		2016-07-29 更新
*/
$id 	= $_GET["id"];
$cbk	= $_GET["callback"];
$json	= "$cbk({\"value\":null})";

if ( !empty( $id) )
{
	$db = new DnFolder();
	$data = $db->childs($id);
	$data = urlencode($data);
	$data = str_replace("+", "%20", $data);
	$json = "$cbk({\"value\":\"$data\"})";
}
echo $json;
?>