<?php
ob_start();
header('Content-type: text/html;charset=utf-8');
/*
	此文件主要功能如下：
		1.在数据库中添加新记录
		2.返回新加记录信息。JSON格式
		3.创建上传目录
	此文件主要在数据库中添加新的记录并返回文件信息
		如果存在则在数据库中添加一条相同记录。返回添加的信息
		如果不存在，则向数据库中添加一条记录。并返回此记录ID
	控件每次计算完文件MD5时都将向信息上传到此文件中
	@更新记录：
		2023-02-18 优化
		2017-07-11 优化
		2014-08-12 完成逻辑。
*/

require_once('../../db/head.php');

$wb = new WebBase();

$pid            = $wb->queryString("pid");
$pidRoot        = $wb->queryString("pidRoot");
$md5 			= $_GET["md5"];
$id 			= $_GET["id"];
$uid 			= $_GET["uid"];
$lenLoc			= $_GET["lenLoc"];//10240
$sizeLoc		= $_GET["sizeLoc"];//10mb
$sizeLoc		= str_replace("+", " ", $sizeLoc);
$blockSize      = $wb->reqString("blockSize", "5242880");
$blockSizeSec   = $wb->reqInt("blockSizeSec");//块加密大小
$callback 		= $_GET["callback"];//jsonp
$pathLoc		= $_GET["pathLoc"];
$pathLoc		= PathTool::urldecode_path($pathLoc);
$encryptAgo     = $wb->queryString("encryptAgo");
$lenLocSec      = $wb->reqInt("lenLocSec");//文件加密大小

if(    empty($md5)||
	strlen($uid)<1||
	empty($sizeLoc))
{
	echo $callback . "({\"value\":null})";
	die();
}

$ext = PathTool::getExtention($pathLoc);
$fileSvr = new FileInf();
$fileSvr->id = $id;
$fileSvr->pid = $pid;
$fileSvr->pidRoot = $pidRoot;
$fileSvr->fdChild = false;
$fileSvr->fdTask = false;
$fileSvr->nameLoc = PathTool::getName($pathLoc);
$fileSvr->pathLoc = $pathLoc;
$fileSvr->nameSvr = $fileSvr->nameLoc;
$fileSvr->lenLoc = intval($lenLoc);
$fileSvr->lenLocSec = $lenLocSec;
$fileSvr->sizeLoc = $sizeLoc;
$fileSvr->deleted = false;
$fileSvr->md5 = $md5;
$fileSvr->uid = $uid;
$fileSvr->blockSize = intval($blockSize);
$fileSvr->blockSizeSec = $blockSizeSec;
$fileSvr->encrypt = ConfigReader::storageEncrypt();//存储加密
$fileSvr->encryptAgo = $encryptAgo;//加密算法，sm4,aes

//生成路径
$pb = new PathBuilderUuid();
$fileSvr->pathSvr = $pb->genFile($uid,$fileSvr);
$fileSvr->pathSvr = str_replace("\\", "/", $fileSvr->pathSvr);

$db = new DBFile();
$fileExist = new FileInf();

//数据库存在相同文件
if ($db->exist_file($md5, $fileExist))
{
	$fileSvr->nameSvr = $fileExist->nameSvr;
	$fileSvr->pathSvr = $fileExist->pathSvr;
	$fileSvr->perSvr = $fileExist->perSvr;
	$fileSvr->lenSvr = intval($fileExist->lenSvr);
	$fileSvr->complete = (bool)$fileExist->complete;
	$fileSvr->encrypt = $fileExist->encrypt;
	$fileSvr->encryptAgo = $fileExist->encryptAgo;
	$fileSvr->lenLoc = $fileExist->lenLoc;
	$fileSvr->lenLocSec = $fileExist->lenLocSec;
	$fileSvr->blockSize = $fileExist->blockSize;
	$fileSvr->blockSizeSec = $fileExist->blockSizeSec;
	$db->Add($fileSvr);
	
	//触发事件
	up6_biz_event::file_create_same($fileSvr);
}//数据库不存在相同文件
else
{
	$db->Add($fileSvr);
	//触发事件
	up6_biz_event::file_create($fileSvr);
	
	//创建文件
	$fr = ConfigReader::blockWriter();
	$fr->make($fileSvr);
}
	
//将路径转换成相对路径
$fileSvr->pathSvr = $pb->absToRel($fileSvr->pathSvr);
$json = json_encode($fileSvr,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);//

$json = urlencode($json);
$json = str_replace("+","%20",$json);
$json = $callback . "({'value':'$json','ret':true})";//返回jsonp格式数据。
echo $json;
header('Content-Length: ' . ob_get_length());
?>