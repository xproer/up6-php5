<?php
ob_start();
header('Content-Type: text/html;charset=utf-8');
/*
	控件每次向此文件POST数据
	逻辑：
		1.更新数据库进度
		2.将文件块数据保存到服务器中。
	更新记录：
		2017-07-11 
			简化文件块逻辑，
			取消进度更新操作
		2014-04-09 增加文件块验证功能。
*/
require_once('../../db/head.php');

$head = new HttpHeader();

$uid	 		= $head->param("uid");
$fid	 		= $head->param("id");
$pid         	= $head->param("pid");
$pidRoot     	= $head->param("pidRoot");
$md5			= $head->param("md5");
$lenSvr			= $head->param("lenSvr");
$lenLoc			= $head->param("lenLoc");
$encryptAgo		= $head->param("encryptAgo");//加密算法，aes,sm4
$lenLocSec		= $head->paramInt("lenLocSec");//文件加密大小
$blockOffset	= $head->param("blockOffset");
$blockSize      = $head->paramInt("blockSize");//原始块大小
$blockEncrypt   = $head->paramBool("blockEncrypt");//加密标识
$blockIndex		= $head->param("blockIndex");
$blockCount  	= $head->param("blockCount");          //块总数
$blockMd5		= $head->param("blockMd5");
$object_id   = $head->param("object_id");
$complete    = strcmp("true",$head->param("complete"))==0;
$pathSvr     = $_POST["pathSvr"];
$pathRel     = $_POST["pathRel"];
$pathLoc     = $_POST["pathLoc"];
$pathSvr     = PathTool::getBase64($pathSvr);
$pathRel     = PathTool::getBase64($pathRel);
$pathLoc     = PathTool::getBase64($pathLoc);
$token       = $head->param("token");
$pathTmp		= $_FILES['file']['tmp_name'];//

if(empty($pathLoc)) $pathLoc = $_FILES['file']['name'];

//相关参数不能为空
if (   (strlen($lenLoc)>0) 
	&& (strlen($uid)>0) 
	&& (strlen($fid)>0) 
    && (strlen($blockOffset)>0) 
	&& !empty($pathSvr))
{
	$verify = false;
	$msg = "";
	$md5Svr = "";
	$stm = FileTool::readAll($pathTmp);

	$fileSvr = new FileInf();
	$fileSvr->id = $fid;
	$fileSvr->pid = $pid;
	$fileSvr->pidRoot = $pidRoot;
	$fileSvr->object_id = $object_id;
	$fileSvr->lenSvr = $lenSvr;
	$fileSvr->lenLoc = $lenLoc;
	$fileSvr->lenLocSec = $lenLocSec;
	$fileSvr->pathLoc = $pathLoc;
	$fileSvr->pathSvr = $pathSvr;
	$fileSvr->pathRel = $pathRel;
	$fileSvr->blockIndex = intval($blockIndex);
	$fileSvr->blockOffset = intval($blockOffset);
	$fileSvr->blockOffsetCry = $head->paramInt("blockOffsetCry");
	$fileSvr->blockSize = $blockSize;
	$fileSvr->blockSizeSec = $head->paramInt("blockSizeCry");
	$fileSvr->blockSizeCpr = $head->paramInt("blockSizeCpr");
	$fileSvr->blockCprType = $head->param("blockCprType");
	$fileSvr->blockCount = intval($blockCount);
	$fileSvr->blockEncrypt = $blockEncrypt;
	$fileSvr->nameLoc = PathTool::getName($fileSvr->pathLoc);
	$fileSvr->nameSvr = $fileSvr->nameLoc;
	$fileSvr->encrypt = ConfigReader::storageEncrypt();//加密存储
	$fileSvr->encryptAgo = $head->param("blockEncryptAgo");
	$fileSvr->blockPath = $pathTmp;//

	//md5验证
	if(!UtilsTool::check_md5($stm,$blockMd5))
	{
	    $md5Svr = md5($stm);
	    $obj = array(
	        'msg'=>'blockMd5ValidFail',
	        'errCode'=>'blockMd5ValidFail',
	        'md5Svr'=>$md5Svr,
	        'md5Loc'=>$blockMd5,
	        'offset'=>-1
	    );
	    exit(json_encode($obj));
	}
	
	//解压缩
	$stm = UtilsTool::unCompress($stm,$fileSvr);

	//解密
	$stm = UtilsTool::decrypt($stm,$fileSvr);

	//token验证
	$ws = new WebSafe();
	if(!$ws->validToken($token,$fileSvr,"block"))
	{
		$obj = array(
			'msg'=>'tokenValidFail', 
			'errCode'=>'tokenValidFail', 
			'md5'=>"", 
			'offset'=>-1
		);
		exit(json_encode($obj));
	}
	
	$pb = new PathBuilder();
	$fileSvr->pathSvr = $pb->relToAbs($fileSvr->pathSvr);
	
	$needGenId = !empty($pid);
	if($needGenId) $needGenId = 1 == $fileSvr->blockIndex;
	
	try{
		//保存文件块数据
		$fw = ConfigReader::blockWriter();
		if($needGenId) $fileSvr->object_id = $fw->make($fileSvr);
		//保存到层级信息文件
		$fileSvr->saveScheme();
		//写入块数据
		$fileSvr->etag = $fw->write($fileSvr, $stm);
		//合并文件
		if($complete) $fw->writeLastPart($fileSvr);
	}
	catch(Exception $e){
		$obj = array(
			'msg'=>'writeBlockError', 
			'errCode'=>'writeBlockError', 
			'exception'=>$e->getMessage(),
			'offset'=>-1,
		    'fileSvr'=>json_encode($fileSvr,JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE)
		);
		exit(json_encode($obj));
	}
	
	up6_biz_event::file_post_block($fid, $blockIndex);		
	
	$obj = array('msg'=>'ok', 
		'offset'=>$blockOffset,
		'fields'=>array('object_id'=>$fileSvr->object_id)		    
	);
	$msg = json_encode($obj);
	echo $msg;
}
else
{
	$obj = array(
		'msg'=>'paramEmpty', 
		'errCode'=>'paramEmpty', 
		'uid'=>$uid,
		'fid'=>$fid,
		'md5'=>$md5,
		'lenSvr'=>$lenSvr,
		'lenLoc'=>$lenLoc,
		'pathSvr'=>$pathSvr,
		'blockOffset'=>$blockOffset,
		'offset'=>-1
	);
	exit(json_encode($obj));
}
header('Content-Length: ' . ob_get_length());
?>