<?php
ob_start();
header('Content-Type: text/html;charset=utf-8');
/*
	业务逻辑：
		1.向数据库添加文件和文件夹信息
		2.将文件和文件夹ID保存到JSON中
		3.将JSON返回给客户端。
	文件夹上传方法中调用。

	更新记录：
		2014-07-23 创建
		2014-08-05 修复BUG，上传文件夹如果没有子文件夹时报错的问题。
		2014-09-12 完成逻辑。
		2014-09-15 修复设置子文件，子文件夹层级结构错误的问题。
		2016-04-13 以md5模式上传文件夹
		2016-05-29 修复添加文件夹数据错误的问题。
		2017-07-11 取消ID生成操作
		2017-11-14 此页面取消，文件夹上传统一采用uuid模式。

	JSON格式化工具：http://tool.oschina.net/codeformat/json
	POST数据过大导致接收到的参数为空解决方法：http://sishuok.com/forum/posts/list/2048.html
*/
require('../../../db/head.php');

$id 			= $_GET["id"];
$uid 			= $_GET["uid"];
$lenLoc			= $_GET["lenLoc"];//10240
$sizeLoc		= $_GET["sizeLoc"];//10mb
$sizeLoc		= str_replace("+", " ", $sizeLoc);
$callback 		= $_GET["callback"];//jsonp
$pathLoc		= $_GET["pathLoc"];
$pathLoc		= PathTool::urldecode_path($pathLoc);

if(    empty($id)
	|| strlen($uid)<1
	|| empty($pathLoc))
{
	echo $id;
	echo $uid;
	echo $pathLoc;
	echo $callback . "({\"value\":null})";
	die();
}

$fileSvr = new FileInf();
$fileSvr->id = $id;
$fileSvr->fdChild = false;
$fileSvr->fdTask = true;
$fileSvr->uid = $uid;
$fileSvr->nameLoc = PathTool::getName($pathLoc);
$fileSvr->pathLoc = $pathLoc;
$fileSvr->lenLoc = intval($lenLoc);
$fileSvr->sizeLoc = $sizeLoc;
$fileSvr->deleted = false;
$fileSvr->nameSvr = $fileSvr->nameLoc;

//生成路径
$pb = new PathBuilderUuid();
$fileSvr->pathSvr = $pb->genFolder($fileSvr);
$fileSvr->pathSvr = str_replace("\\", "/", $fileSvr->pathSvr);
if(!PathTool::mkdir($fileSvr->pathSvr))
{
    exit("create dir error");
    return;
}

//创建层级信息文件
$fs = new FolderSchema();
if(!$fs->create($fileSvr))
{
    exit("create dir schema error");
    return;
}

//路径转换成相对路径
$fileSvr->pathSvr = $pb->absToRel($fileSvr->pathSvr);
up6_biz_event::folder_create($fileSvr);

$json = $fileSvr->toJson();//低版本php中，json_encode会将汉字进行unicode编码
$json = urlencode($json);

$json = str_replace("+","%20",$json);
$json = $callback . "({'value':'$json'})";//返回jsonp格式数据。
echo $json;
header('Content-Length: ' . ob_get_length());
header("content-type:text/html;charset=utf-8");  //设置编码
?>