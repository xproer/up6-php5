CREATE TABLE IF	NOT EXISTS `up6_folders`(
`f_id`              CHAR(32) NOT NULL,
`f_nameLoc`         VARCHAR(255) DEFAULT '',
`f_pid`             CHAR(32) DEFAULT '',
`f_uid`             VARCHAR(255) DEFAULT '0',
`f_lenLoc`          BIGINT(19) DEFAULT '0',
`f_sizeLoc`         VARCHAR(50) DEFAULT '0',
`f_pathLoc`         VARCHAR(255) DEFAULT '',
`f_pathSvr`         VARCHAR(255) DEFAULT '',
`f_pathRel`         VARCHAR(255) DEFAULT '',
`f_folders`         INT(11) DEFAULT '0',
`f_fileCount`       INT(11) DEFAULT '0',
`f_filesComplete`   INT(11) DEFAULT '0',
`f_complete`        TINYINT(1) DEFAULT '0',
`f_deleted`         TINYINT(1) DEFAULT '0',
`f_time` 			timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
`f_pidRoot` 		char(32) default '',
PRIMARY KEY (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;